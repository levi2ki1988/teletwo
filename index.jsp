<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%--<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>--%>

<html>
<head>
    <meta charset="UTF-8"/>
    <title>Подтверждение оплаты</title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'/>
    <script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
    <style type="text/css">
        /*put your styles here*/
    </style>
</head>

<body>
<table class="payment-info color-grey">
    <tr>
        <td>Услуга:</td>
        <td>Подписка на сервис “1000kino.ru”</td>
    </tr>
    <%= request.getSession().getAttribute("body")%>
</table>

<form class="form" name="chng_frm" method="get" action="<%= (String) request.getSession().getAttribute("urlLocation")%>">
    <input type="checkbox" checked id="checkbox_agreement" onclick="logincheckboxCheck(this);"/>
    <label>Согласен с <a href="http://1000kino.ru/rul.php" target="_blank">правилами предоставления услуги</a></label>
    <br/> Код подтверждения отправлен на&nbsp;номер&nbsp;
    <span><%= request.getSession().getAttribute("telebody")%></span>
    <input name="ButtonReturn" value="" type="hidden"/>
    <input name="serviceId" value="<%= (String) request.getSession().getAttribute("serviceId")%>" type="hidden"/>&emsp;
    <a href="#" onclick="javascript:chng_frm.submit();" class="resend text14 color-white hover-blue">Изменить номер</a>
    <br/>
    <span><%= request.getSession().getAttribute("deactivate")%></span>
</form>

<form name="frm" method="get" action="<%= (String) request.getSession().getAttribute("urlLocation")%>" onkeypress="if (event.keyCode == 13) return false;">
    <label for="sms">Введите полученный код</label>
    <input type="hidden" name="resultForm" value=""/>
    <input type="text" name="sms" id="sms" size="10" value="" data-required/>
    <a href="#" onclick="javascript:frm.resultForm.value = 'repeat_sms_code'; javascript: frm.submit()">Отправить повторно</a>

    <div class="error-text error-sms">Неверный SMS-код</div>
    <div class="err-msg-server">
        <%= request.getSession().getAttribute("errText")%>
    </div>
    <div class="success-msg-server">
        <%= request.getSession().getAttribute("successText")%>
    </div>

    <input type="hidden" name="ServiceId" value="<%= (String) request.getSession().getAttribute("serviceId")%>"/>
    <input type="hidden" name="ButtonOK" valuе=""/>
    <button id="agree" type="submit" onclick="javascript:frm.ButtonOK.value = 'OK'; javascript:frm.resultForm.value = 'OK'">
        Подписаться
    </button>
    <a href="#" onclick="javascript:frm.ButtonOK.setAttribute('name', 'ButtonCancel');
                                            javascript:frm.ButtonCancel.value = 'Cancel';
                                            javascript:frm.resultForm.value = 'OK';
                                            javascript:frm.submit();">
        Отмена
    </a>
</form>
</body>
</html>
