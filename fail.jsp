<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%--<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>--%>

<html>
<head>
    <title>Ошибка</title>
    <style type="text/css">
        /*put your styles here*/
    </style>
</head>
<body>
<div class="page">
    <article class="container">
        <section class="main-content">
            <div class="note">
                <div class="avgnum color-white"><%=request.getSession().getAttribute("errText")%>
                </div>
                <div class="avgnum1 color-white"><%=request.getSession().getAttribute("successText")%>
                </div>
                <div class="alink color-white">
                    <br/><%= request.getSession().getAttribute("fUrl")%> <br/>
                </div>

            </div>
        </section>
    </article>
</div>
</body>
</html>