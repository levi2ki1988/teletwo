<!DOCTYPE html>
<%--<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>--%>
<html>
<head>
    <title>Подтверждение оплаты</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="google" content="notranslate"/>
    <meta name="format-detection" content="telephone=no"/>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'/>
    <meta charset="UTF-8"/>
    <style type="text/css">
        /* put your style here */
    </style>

    <%@ page contentType="text/html;charset=UTF-8" language="java" %>


</head>
<body>
<table class="payment-info">
    <%= request.getSession().getAttribute("body")%>
</table>

<form name="frm" id="form" method="get" action="<%= (String) request.getSession().getAttribute("urlLocation")%>" onsubmit="return prepareForm(this);">
    <input type="hidden" name="resultForm" value="">
    <input type="text" name="msisdn_formatted" placeholder="+7 (___) ___-__-__" data-validate="phone" data-required="true" value="<%=request.getSession().getAttribute("msisdn_cp")%>"/>
    <input type="hidden" name="msisdn" value="">
    <input type="hidden" name="msisdn_cp" value="<%= (String) request.getSession().getAttribute("msisdn_cp")%>">
    <input type="hidden" name="ButtonOK" valuе="">
    <input type="hidden" name="ServiceId" value="<%= (String) request.getSession().getAttribute("serviceId")%>">
    <button class="button button-confirm" type="submit" onclick="javascript:frm.ButtonOK.value = 'OK';
                                                            javascript:frm.msisdn_formatted.setAttribute('disabled', true);
                                                            javascript:frm.resultForm.value = 'OK'" value="OK">
        Подтвердить
    </button>
    <a href="#" class="button button-cancel" onclick="javascript:frm.ButtonOK.setAttribute('name', 'ButtonCancel');
                                                            javascript:frm.ButtonCancel.value = 'Cancel';
                                                            javascript:frm.resultForm.value = 'OK';
                                                            javascript:frm.msisdn_formatted.setAttribute('disabled', true);
                                                            javascript:frm.submit();">Отмена</a>
</form>
</body>
</html>
