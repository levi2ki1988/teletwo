<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:forward page="/redirect"></jsp:forward>
<html>
<head>
    <script type="text/javascript">
        window.history.forward();

        function noBack() {
            window.history.forward();
        }
    </script>
</head>
<body onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload=""></body>
</html>